﻿using System.ComponentModel.DataAnnotations;

namespace ForeverGame.Web.Models
{
    public class UserGetTokenRequest
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

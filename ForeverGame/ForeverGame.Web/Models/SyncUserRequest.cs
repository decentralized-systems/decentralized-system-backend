﻿using System.ComponentModel.DataAnnotations;
using ForeverGame.Data.Entities;

namespace ForeverGame.Web.Models
{
    public class SyncItemRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ItemColor[] Colors { get; set; }

        public int Score { get; set; }
    }

    public class SyncUserRequest
    {
        [Required]
        public int UserId { get; set; }

        public int[] ItemIds { get; set; }
    }
}

﻿using System;
using Newtonsoft.Json;

namespace ForeverGame.Web.Models
{
    public interface ISuccessResponse
    {

    }

    public class ErrorResponse : IEquatable<ErrorResponse>
    {
        public string Code { get; set; }

        public string Message { get; set; }

        #region IEquatable
        public bool Equals(ErrorResponse other)
        {
            return Equals(Code, other.Code) && Equals(Message, other.Message);
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != GetType()) return false;
            return Equals(other as ErrorResponse);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Code, Message);
        }
        #endregion
    }

    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ApiResponse : IEquatable<ApiResponse> 
    {
        [JsonProperty(PropertyName = "error")]
        public ErrorResponse ErrorResponse { get; set; }

        [JsonProperty(PropertyName = "response")]
        public ISuccessResponse SuccessResponse { get; set; }

        public static ApiResponse Ok(ISuccessResponse response)
        {
            return new ApiResponse { SuccessResponse = response };
        }

        public static ApiResponse Error(string code)
        {
            return new ApiResponse { ErrorResponse = new ErrorResponse { Code = code } };
        }

        public static ApiResponse Error(string code, Exception exception)
        {
            return new ApiResponse { ErrorResponse = new ErrorResponse { Code = code, Message = exception.Message } };
        }

        public static ApiResponse Error(string code, string exception)
        {
            return new ApiResponse { ErrorResponse = new ErrorResponse { Code = code, Message = exception } };
        }

        #region IEquatable
        public bool Equals(ApiResponse other)
        {
            return Equals(ErrorResponse, other.ErrorResponse) && Equals(SuccessResponse, other.SuccessResponse);
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != GetType()) return false;
            return Equals(other as ApiResponse);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ErrorResponse, SuccessResponse);
        }
        #endregion
    }
}

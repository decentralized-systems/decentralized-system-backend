﻿using System.Collections.Generic;
using ForeverGame.Data.Entities;
using ForeverGame.Web.Services;

namespace ForeverGame.Web.Models
{
    public class GameStateResponse
    {
        public GameCellResponse[] Field { get; set; }
        public Dictionary<string, int> Statistic { get; set; }
    }

    public class ItemResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ItemColor[] Colors { get; set; }

        public int Score { get; set; }

        public UserResponse User { get; set; }
    }

    public class UserResponse
    {
        public int Id { get; set; }

        public string Login { get; set; }
    }

    public class GameCellResponse
    {
        public Coordinates Coordinates { get; set; }
        public ItemResponse Item { get; set; }
        public UserResponse Player { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace ForeverGame.Web.Models
{
    public class UserRegisterRequest
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

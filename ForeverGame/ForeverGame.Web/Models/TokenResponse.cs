﻿namespace ForeverGame.Web.Models
{
    public class TokenResponse : ISuccessResponse
    {
        public string Token { get; set; }
        public int UserId { get; set; }
    }
}

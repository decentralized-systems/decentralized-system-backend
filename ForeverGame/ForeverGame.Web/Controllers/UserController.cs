﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using ForeverGame.Data.Exceptions;
using ForeverGame.Web.Attributes;
using ForeverGame.Web.Mapper;
using ForeverGame.Web.Models;
using ForeverGame.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ForeverGame.Web.Controllers
{
    
    [Produces("application/json")]
    [Route("api/users")]
    [ModelValidation]
    public class UsersController : Controller
    {
        private readonly IUserService userService;
        private readonly IItemMapper itemMapper;
        private readonly ILogger<UsersController> logging;

        public UsersController(IUserService userService, IItemMapper itemMapper, ILogger<UsersController> logging)
        {
            this.userService = userService;
            this.itemMapper = itemMapper;
            this.logging = logging;
        }

        [AllowAnonymous]
        [HttpPost("auth")]
        [ProducesResponseType(typeof(TokenResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<TokenResponse>> GetToken([FromBody] UserGetTokenRequest request)
        {
            try
            {
                var serviceResponse = await userService.GetToken(request.Login, request.Password);
                return Ok(ApiResponse.Ok(new TokenResponse
                    {Token = serviceResponse.Token, UserId = serviceResponse.UserId}));
            }
            catch (AuthenticationException e)
            {
                return StatusCode(StatusCodes.Status401Unauthorized,
                    ApiResponse.Error(ErrorStatusCodes.NotAuthorized, e));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.BadRequest, e));
            }
        }

        [AllowAnonymous]
        [HttpPost("register")]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Register([FromBody] UserRegisterRequest request)
        {
            try
            {
                await userService.Register(request.Login, request.Password);
                return Ok();
            }
            catch (UnsafeUserPasswordException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.UnsafePassword, e));
            }
            catch (UserExistsException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.EntityExists, e));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.BadRequest, e));
            }
        }

        [Authorize]
        [HttpPost("sync")]
        [ProducesResponseType(typeof(SyncUserRequest), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> Sync([FromBody] SyncUserRequest request)
        {
            try
            {
                await userService.SyncItems(request.UserId, request.ItemIds);
                return Ok();
            }
            catch (UnsafeUserPasswordException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.UnsafePassword, e));
            }
            catch (UserExistsException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.EntityExists, e));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status400BadRequest,
                    ApiResponse.Error(ErrorStatusCodes.BadRequest, e));
            }
        }
    }
}

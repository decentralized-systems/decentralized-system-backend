﻿using System.Linq;
using ForeverGame.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ForeverGame.Web.Attributes
{
    public class ModelValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid) return;

            var message = string.Join("; ",
                context.ModelState.Select(x => string.Join(' ', x.Value.Errors.Select(y => y.ErrorMessage))));
            var response = ApiResponse.Error(ErrorStatusCodes.BadRequest, message);
            var result = new ObjectResult(response)
            {
                StatusCode = StatusCodes.Status422UnprocessableEntity
            };
            context.Result = result;
        }
    }
}

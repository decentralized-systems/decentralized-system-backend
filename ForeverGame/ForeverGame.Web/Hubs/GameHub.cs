﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ForeverGame.Web.Mapper;
using ForeverGame.Web.Models;
using ForeverGame.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace ForeverGame.Web.Hubs
{
    [Authorize]
    public class GameHub : Hub
    {
        private readonly IGameMapper gameMapper;
        private readonly IGameService gameService;
        private readonly IItemMapper itemMapper;
        private readonly ILogger<GameHub> logger;

        public GameHub(IGameMapper gameStateMapper, IGameService gameService, IItemMapper itemMapper, ILogger<GameHub> logger)
        {
            this.gameMapper = gameStateMapper;
            this.gameService = gameService;
            this.itemMapper = itemMapper;
            this.logger = logger;
        }

        private async Task GetGameState()
        {
            try
            {
                var state = await gameService.GetState();
                var dtoState = gameMapper.ConvertToResponse(state);
                await Clients.All.SendAsync("GetState", dtoState);
            }
            catch (Exception e)
            {
                logger.LogError( $"{e.Message} || {e.StackTrace}");
            }
        }

        public void SendTestMessage()
        {
            Thread.Sleep(500);
            Clients.All.SendAsync("TestMessage", "Hello");
        }

        public override async Task OnConnectedAsync()
        {
            try
            {
                var name = Context.User.Identity.Name;
                await gameService.ConnectUser(int.Parse(name));
                logger.LogInformation($"User {name} connected to server");
            }
            catch (Exception e)
            {
                logger.LogError($"{e.Message} || {e.StackTrace}");
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                var name = Context.User.Identity.Name;
                await gameService.DisconnectUser(int.Parse(name));
                logger.LogInformation($"Disconnected from server with e: {exception.Message}");
            }
            catch (Exception e)
            {
                logger.LogError($"{e.Message} || {e.StackTrace}");
            }
        }

        public async Task Move(int userId, MovementDirection movementDirection)
        {
            try
            {
                await gameService.Move(userId, movementDirection);
                await GetGameState();
            }
            catch (Exception e)
            {
                logger.LogError($"{e.Message} || {e.StackTrace}");
            }
        }

        public async Task<ItemResponse> GrabItem(int userId)
        {
            ItemResponse response;
            try
            {
                var item = await gameService.GrabItem(userId);
                await GetGameState();
                response = itemMapper.ConvertItemToResponse(item);
            }
            catch (Exception e)
            {
                logger.LogError($"{e.Message} || {e.StackTrace}");
                return null;
            }

            return response;
        }

        public async Task CombineItems(int userId, int firstItemId, int secondItemId)
        {
            try
            {
                await gameService.CombineItems(userId, firstItemId, secondItemId);
                await GetGameState();
            }
            catch (Exception e)
            {
                logger.LogError($"{e.Message} || {e.StackTrace}");
            }
        }
    }
}

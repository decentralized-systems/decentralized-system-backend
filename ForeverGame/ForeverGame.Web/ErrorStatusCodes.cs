﻿namespace ForeverGame.Web
{
    public class ErrorStatusCodes
    {
        public static string NotFound => "notFound";
        public static string UnsafePassword => "unsafePassword";
        public static string EntityExists => "entityExists";
        public static string PermissionDenied => "permissionDenied";
        public static string MethodNotAllowed => "methodNotAllowed";
        public static string NotAuthorized => "notAuthorized";
        public static string TooLongName => "tooLongName";
        public static string BadRequest => "badRequest";
        public static string NotImplemented => "notImplemented";
    }
}

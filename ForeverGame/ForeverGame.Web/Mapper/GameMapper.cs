﻿using System;
using System.Linq;
using ForeverGame.Data.Entities;
using ForeverGame.Web.Models;
using ForeverGame.Web.Services;

namespace ForeverGame.Web.Mapper
{
    public interface IGameMapper
    {
        GameStateResponse ConvertToResponse(GameState gameState);
    }
    public class GameMapper : IGameMapper
    {
        private readonly IItemMapper itemMapper;

        public GameMapper(IItemMapper itemMapper)
        {
            this.itemMapper = itemMapper;
        }

        private UserResponse ConvertUserToResponse(User user)
        {
            return new UserResponse
            {
                Id = user.Id,
                Login = user.Login
            };
        }

        public GameStateResponse ConvertToResponse(GameState gameState)
        {
            return new GameStateResponse
            {
                Statistic = gameState.Statistic,
                Field = gameState.Field.Cast<Cell>()
                    .Select(x =>
                    {
                        var gameCell = new GameCellResponse {Coordinates = x.Coordinates};
                        if (x.Item != null)
                        {
                            gameCell.Item = itemMapper.ConvertItemToResponse(x.Item);
                            gameCell.Item.User = ConvertUserToResponse(x.Player);
                        }

                        if (x.Player != null)
                        {
                            gameCell.Player = ConvertUserToResponse(x.Player);
                        }

                        return gameCell;
                    }).ToArray()
            };
        }
    }
}

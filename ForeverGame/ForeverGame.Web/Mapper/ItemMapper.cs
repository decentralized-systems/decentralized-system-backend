﻿using ForeverGame.Data.Entities;
using ForeverGame.Web.Models;

namespace ForeverGame.Web.Mapper
{
    public interface IItemMapper
    {
        ItemResponse ConvertItemToResponse(Item item);
        Item ConvertRequestToItem(SyncItemRequest request);
    }

    public class ItemMapper : IItemMapper
    {
        public ItemResponse ConvertItemToResponse(Item item)
        {
            return new ItemResponse
            {
                Colors = item.Colors,
                Description = item.Description,
                Name = item.Name,
                Id = item.Id,
                Score = item.Score
            };
        }

        public Item ConvertRequestToItem(SyncItemRequest request)
        {
            return new Item
            {
                Colors = request.Colors,
                Id = request.Id,
                Description = request.Description,
                Name = request.Name,
                Score = request.Score
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ForeverGame.Data.Entities;
using ForeverGame.Data.Repositories;

namespace ForeverGame.Web.Services
{
    public enum MovementDirection
    {
        Up, Down, Left, Right
    }

    public class Coordinates
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class Cell
    {
        public Coordinates Coordinates { get; set; }
        public Item Item { get; set; }
        public User Player { get; set; }
    }

    public class GameState
    {
        public Cell[,] Field { get; set; }
        public Dictionary<int, Coordinates> UsersPositions { get; set; }
        public Dictionary<string, int> Statistic { get; set; }
    }

    public interface IGameService
    {
        Task DisconnectUser(int userId);
        Task ConnectUser(int userId);
        Task Move(int userId, MovementDirection movementDirection);
        Task<Item> GrabItem(int userId, bool isAutoCombine = false);
        Task SwapItems(int fromUserId, int toUserId, int itemId);
        Task CombineItems(int userId, int firstItemId, int secondItemId);
        Task<GameState> GetState();
    }

    public class GameService : IGameService
    {
        private readonly IUserRepository userRepository;
        private readonly IItemRepository itemRepository;

        private readonly IItemProvider itemProvider;
        private readonly Random randomProvider;

        private GameState gameState;
        private const int FieldSize = 10;
        private const int UserItemLimit = 6;

        public GameService(IUserRepository userRepository, IItemRepository itemRepository, IItemProvider itemProvider)
        {
            this.randomProvider = new Random();
            itemRepository.ClearUnusedItems().GetAwaiter().GetResult();

            var field = new Cell[FieldSize, FieldSize];
            for (var x = 0; x < FieldSize; x++)
            {
                for (var y = 0; y < FieldSize; y++)
                {
                    var cell = new Cell
                    {
                        Coordinates = new Coordinates {X = x, Y = y},
                    };
                    if (randomProvider.NextDouble() > 0.7)
                    {
                        cell.Item = itemProvider.GenerateItem(randomProvider);
                        itemRepository.Add(cell.Item).GetAwaiter().GetResult();
                    }

                    field[x, y] = cell;
                }
            }

            this.gameState = new GameState
            {
                Field = field,
                Statistic = new Dictionary<string, int>(),
                UsersPositions = new Dictionary<int, Coordinates>()
            };
            this.userRepository = userRepository;
            this.itemRepository = itemRepository;
            this.itemProvider = itemProvider;
            
        }

        public async Task DisconnectUser(int userId)
        {
            var pos = gameState.UsersPositions[userId];
            gameState.Field[pos.X, pos.Y] = null;
            gameState.UsersPositions.Remove(userId);
        }

        public async Task ConnectUser(int userId)
        {
            var user = await userRepository.Get(userId);
            if (user == null) throw new ArgumentException("User not found.");

            int x, y;
            do
            {
                x = randomProvider.Next(FieldSize);
                y = randomProvider.Next(FieldSize);
            } while (gameState.Field[x,y].Player == null);

            gameState.Field[x, y].Player = user;

            gameState.UsersPositions.Add(userId, new Coordinates {X = x, Y = y});

            if (!gameState.Statistic.ContainsKey(user.Login))
            {
                gameState.Statistic.Add(user.Login, 0);
            }
        }

        public async Task Move(int userId, MovementDirection movementDirection)
        {
            if (!gameState.UsersPositions.TryGetValue(userId, out var coordinates))
            {
                throw new ArgumentException();
            }

            Coordinates newCoordinates = null;
            switch (movementDirection)
            {
                case MovementDirection.Down:
                    if (coordinates.Y + 1 > FieldSize) break;
                    newCoordinates = new Coordinates {X = coordinates.X, Y = coordinates.Y + 1};
                    break;
                case MovementDirection.Up:
                    if (coordinates.Y - 1 < 0) break;
                    newCoordinates = new Coordinates { X = coordinates.X, Y = coordinates.Y - 1 };
                    break;
                case MovementDirection.Left:
                    if (coordinates.X - 1 < 0) break;
                    newCoordinates = new Coordinates { X = coordinates.X- 1, Y = coordinates.Y };
                    break;
                case MovementDirection.Right:
                    if (coordinates.X + 1 < 0) break;
                    newCoordinates = new Coordinates { X = coordinates.X + 1, Y = coordinates.Y };
                    break;
            }

            if (newCoordinates != null)
            {
                gameState.Field[newCoordinates.X, newCoordinates.Y].Player = gameState.Field[coordinates.X, coordinates.Y].Player;
                gameState.Field[coordinates.X, coordinates.Y].Player = null;
                gameState.UsersPositions[userId] = newCoordinates;
            }
        }

        public async Task<Item> GrabItem(int userId, bool isAutoCombine = false)
        {
            var user = await userRepository.Get(userId);
            if (user == null) throw new ArgumentException("User not found.");

            if (user.Items.Count >= UserItemLimit)
                throw new ArgumentException("User already has maximum number of items.");

            var userPos = gameState.UsersPositions[userId];
            var item = gameState.Field[userPos.X, userPos.Y].Item;
            if (item == null) throw new ArgumentException("Item not found.");

            await itemRepository.ChangeOwner(item.Id, userId);

            gameState.Statistic[user.Login] += item.Score;

            if (!isAutoCombine) return item;

            return item;
        }

        public async Task SwapItems(int fromUserId, int toUserId, int itemId)
        {
            throw new System.NotImplementedException();
        }

        public async Task CombineItems(int userId, int firstItemId, int secondItemId)
        {
            var user = await userRepository.Get(userId);
            if (user == null) throw new ArgumentException("User not found.");

            var firstItem = await itemRepository.Get(firstItemId);
            if (firstItem == null) throw new ArgumentException("Item not found.");

            var secondItem = await itemRepository.Get(secondItemId);
            if (secondItem == null) throw new ArgumentException("Item not found.");

            if (!firstItem.Colors.All(secondItem.Colors.Contains))
                throw new ArgumentException("Items cannot be combined.");

            var newItem = itemProvider.CombineItems(firstItem, secondItem, userId, randomProvider);

            await itemRepository.Delete(firstItem);
            await itemRepository.Delete(secondItem);
            await itemRepository.Add(newItem);

            gameState.Statistic[user.Login] += newItem.Score;
        }

        public async Task<GameState> GetState() => gameState;
    }
}

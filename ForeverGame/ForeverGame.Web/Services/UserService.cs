﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ForeverGame.Data.Entities;
using ForeverGame.Data.Repositories;
using Microsoft.IdentityModel.Tokens;

namespace ForeverGame.Web.Services
{
    public class UserServiceResponse
    {
        public string Token { get; set; }
        public int UserId { get; set; }
    }

    public interface IUserService
    {
        Task<UserServiceResponse> GetToken(string username, string password);
        Task Register(string userName, string password);
        Task SyncItems(int userId, int[] itemIds);
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IItemRepository itemRepository;

        public UserService(IUserRepository userRepository, IItemRepository itemRepository)
        {
            this.userRepository = userRepository;
            this.itemRepository = itemRepository;
        }

        public async Task<UserServiceResponse> GetToken(string username, string password)
        {
            var user = await userRepository.Get(username);
            if (user == null || user.Password != password) throw new AuthenticationException("Login or password are not valid.");

            var tokenHandler = new JwtSecurityTokenHandler();
            var secret = Environment.GetEnvironmentVariable("JwtSecret");
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new []
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return new UserServiceResponse {Token = tokenHandler.WriteToken(token), UserId = user.Id};
        }

        public async Task Register(string userName, string password)
        {
            await userRepository.Add(new User {Login = userName, Password = password});
        }

        public async Task SyncItems(int userId, int[] itemIds)
        {
            var user = await userRepository.Get(userId);
            if (user == null) throw new ArgumentException("User not found");

            var userItemsIds = user.Items.Select(x => x.Id).ToArray();
            foreach (var itemId in itemIds)
            {
                if (userItemsIds.Contains(itemId)) continue;

                var item = await itemRepository.Get(itemId);
                await userRepository.AddItem(userId, item);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForeverGame.Data.Entities;

namespace ForeverGame.Web.Services
{
    public interface IItemProvider
    {
        Item CombineItems(Item firstItem, Item secondItem, int userId, Random randomProvider);
        Item GenerateItem(Random randomProvider);
    }

    public class ItemProvider : IItemProvider
    {
        private readonly ItemColor[] itemColors;
        private readonly string[] words;

        public ItemProvider()
        {
            itemColors = Enum.GetValues(typeof(ItemColor)).Cast<ItemColor>().ToArray();
            words = new[]
            {
                "lorem", "ipsum", "dolor", "sit", "amet", "consectetuer",
                "adipiscing", "elit", "sed", "diam", "nonummy", "nibh", "euismod",
                "tincidunt", "ut", "laoreet", "dolore", "magna", "aliquam", "erat"
            };
        }

        private string GetName(Random randomProvider, int numberOfWords = 1)
        {
            var sentenceBuilder = new StringBuilder();

            for (var i = 0; i < numberOfWords; i++)
            {
                if (i > 0 && i < numberOfWords) { sentenceBuilder.Append(" "); }

                var word = words[randomProvider.Next(words.Length)];
                word = word.First().ToString().ToUpper() + word.Substring(1);
                sentenceBuilder.Append(word);
            }

            return sentenceBuilder.ToString();
        }


        private ItemColor GetRandomColor(Random randomProvider)
        {
            return itemColors[randomProvider.Next(itemColors.Length)];
        }

        public Item CombineItems(Item firstItem, Item secondItem, int userId, Random randomProvider)
        {
            var newColors = new List<ItemColor>();
            newColors.AddRange(firstItem.Colors);
            newColors.Add(GetRandomColor(randomProvider));

            return new Item
            {
                Colors = newColors.ToArray(),
                UserId = userId,
                Name = $"{firstItem.Name} {secondItem.Name}",
                Description = $"Item crafted from {firstItem.Name} and {secondItem.Name} items.",
                Score = firstItem.Score * secondItem.Score
            };
        }

        public Item GenerateItem(Random randomProvider)
        {
            var randomChooser = randomProvider.NextDouble();
            var colorNumbers = randomChooser > 0.90 ? 2 : 1;
            var colors = Enumerable.Range(0, colorNumbers)
                .Select(_ => GetRandomColor(randomProvider))
                .ToArray();

            return new Item
            {
                Colors = colors,
                Name = GetName(randomProvider, 1),
                Score = randomProvider.Next(10, 100)
            };
        }
    }
}

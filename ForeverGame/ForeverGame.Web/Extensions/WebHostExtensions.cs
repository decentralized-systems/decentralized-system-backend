﻿using ForeverGame.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace ForeverGame.Web.Extensions
{
    public static class WebHostExtensions
    {
        public static IWebHost InitializeDatabase(this IWebHost webHost)
        {
            using (var serviceScope = webHost.Services.CreateScope())
            {
                var dbInitializer = serviceScope.ServiceProvider.GetService<IDbInitializer>();
                dbInitializer.Migrate();
                dbInitializer.Seed();
            }

            return webHost;
        }
    }
}

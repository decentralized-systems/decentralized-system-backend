﻿using ForeverGame.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace ForeverGame.Web.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UpdateDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                var init = serviceScope.ServiceProvider.GetService<IDbInitializer>();
                init.Migrate();
                init.Seed();
            }
        }
    }
}

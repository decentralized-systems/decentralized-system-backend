﻿using System;
using ForeverGame.Data.Extensions;
using ForeverGame.Data.Repositories;
using ForeverGame.Web.Extensions;
using ForeverGame.Web.Hubs;
using ForeverGame.Web.Mapper;
using ForeverGame.Web.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace ForeverGame.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        private static void StartupConfigureServices(IServiceCollection services, Func<string> connectionStringBuilder)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.SetupDatabase(connectionStringBuilder);
            services.SetupAuthentication();
            services.SetupSwagger();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IGameMapper, GameMapper>();
            services.AddScoped<IItemProvider, ItemProvider>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IItemMapper, ItemMapper>();
            services.AddSingleton<IGameService, GameService>();

            services.AddSignalR();

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin();
            }));
        }

        public void ConfigureProductionServices(IServiceCollection services)
        {
            StartupConfigureServices(services, () =>
            {
                var databaseUrl = Environment.GetEnvironmentVariable("DATABASE_URL");
                var databaseUri = new Uri(databaseUrl);
                var userInfo = databaseUri.UserInfo.Split(':');

                var conStrBuilder = new NpgsqlConnectionStringBuilder
                {
                    Database = databaseUri.LocalPath.TrimStart('/'),
                    Host = databaseUri.Host,
                    Port = databaseUri.Port,
                    Username = userInfo[0],
                    Password = userInfo[1],
                    Pooling = true,
                    UseSslStream = true,
                    SslMode = SslMode.Require,
                    TrustServerCertificate = true
                };

                return conStrBuilder.ToString();
            });
        }

        // This method gets called by the runtime in development environment. Use this method to add services to the container.
        // ReSharper disable once UnusedMember.Global
        public void ConfigureDevelopmentServices(IServiceCollection services)
        {
            StartupConfigureServices(services, () =>
            {
                var conStrBuilder = new NpgsqlConnectionStringBuilder();
                return conStrBuilder.ReadFromJson("developing_connection_string.json").ToString();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Forever Game Api 1.0");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseCors("CorsPolicy");

            app.UpdateDatabase();

            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<GameHub>("/gameHub");
            });
        }
    }
}

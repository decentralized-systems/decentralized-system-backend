﻿using System.Threading.Tasks;
using ForeverGame.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace ForeverGame.Data.Repositories
{
    public interface IItemRepository
    {
        Task<Item> Get(int itemId);
        Task Add(Item item);
        Task ChangeOwner(int itemId, int newUserId);
        Task Delete(Item item);
        Task ClearUnusedItems();
    }

    public class ItemRepository : IItemRepository
    {
        private readonly ApplicationDbContext applicationDbContext;

        public ItemRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task<Item> Get(int itemId)
        {
            return await applicationDbContext.Items.FirstOrDefaultAsync(x => x.Id == itemId);
        }

        public async Task Delete(Item item)
        {
            applicationDbContext.Items.Remove(item);
            await applicationDbContext.SaveChangesAsync();
        }

        public async Task Add(Item item)
        {
            applicationDbContext.Items.Add(item);
            await applicationDbContext.SaveChangesAsync();
        }

        public async Task ClearUnusedItems()
        {
            foreach (var item in applicationDbContext.Items.Include(x => x.User))
            {
                if (item.User == null) applicationDbContext.Remove(item);
            }

            await applicationDbContext.SaveChangesAsync();
        }

        public async Task ChangeOwner(int itemId, int newUserId)
        {
            var item = await Get(itemId);
            item.UserId = newUserId;
        }
    }
}

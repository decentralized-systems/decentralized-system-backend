﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ForeverGame.Data.Entities;
using ForeverGame.Data.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace ForeverGame.Data.Repositories
{
    public interface IUserRepository
    {
        Task<User> Get(string userName);
        Task Add(User user);
        Task<User> Get(int userId);
        Task AddItem(int userId, Item item);
    }

    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public UserRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<User> Get(string userName)
        {
            return await _applicationDbContext.Users
                .Include(x =>x.Items)
                .SingleOrDefaultAsync(x => x.Login == userName);
        }

        public async Task<User> Get(int userId)
        {
            return await _applicationDbContext.Users
                .Include(x => x.Items)
                .SingleOrDefaultAsync(x => x.Id == userId);
        }

        public async Task AddItem(int userId, Item item)
        {
            var oldUser = await Get(userId);
            if (oldUser.Items == null) oldUser.Items = new List<Item>();

            oldUser.Items.Add(item);
            _applicationDbContext.Update(oldUser);
            await _applicationDbContext.SaveChangesAsync();
        }

        public async Task Add(User user)
        {
            var doExist = await _applicationDbContext.Users.AnyAsync(x => x.Login == user.Login);
            if (doExist) throw new UserExistsException();

            if (string.IsNullOrWhiteSpace(user.Password) || user.Password.Length < 6) throw new UnsafeUserPasswordException();

            _applicationDbContext.Add(user);

            await _applicationDbContext.SaveChangesAsync();
        }
    }
}

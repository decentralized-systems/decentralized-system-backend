﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Npgsql;

namespace ForeverGame.Data.Extensions
{
    public static class NpgsqlConnectionStringBuilderExtension
    {
        public static NpgsqlConnectionStringBuilder ReadFromJson(this NpgsqlConnectionStringBuilder builder, string path)
        {
            var connectionFileContent = File.ReadAllText(path);
            var connectionParameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(connectionFileContent);

            builder.Host = connectionParameters["POSTGRES_HOST"];
            builder.Port = int.Parse(connectionParameters["POSTGRES_PORT"]);
            builder.Username = connectionParameters["POSTGRES_USER_ID"];
            builder.Database = connectionParameters["POSTGRES_DB"];
            builder.Pooling = true;

            return builder;
        }
    }
}

﻿using System;
using System.Linq;
using ForeverGame.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace ForeverGame.Data.Extensions
{
    public static class ModelBuilderExtension
    {
        public static void BuildSequence(this ModelBuilder builder)
        {
            builder.HasSequence<int>("OrderNumbers")
                .HasMin(0)
                .StartsAt(0)
                .IncrementsBy(1);
        }

        private static void InitializeBuilder<T>(this ModelBuilder builder) where T : class, IEntity
        {
            builder.Entity<T>()
                .HasKey(x => x.Id);
            builder.Entity<T>()
                .Property(x => x.Id)
                .HasDefaultValueSql("nextval('\"OrderNumbers\"')");
        }

        public static void BuildUsers(this ModelBuilder builder)
        {
            builder.InitializeBuilder<User>();

            builder.Entity<User>()
                .Property(x => x.Password)
                .HasMaxLength(32)
                .IsRequired();

            builder.Entity<User>()
                .Property(x => x.Login)
                .HasMaxLength(32)
                .IsRequired();

            builder.Entity<User>()
                .HasMany(x => x.Items)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);
        }

        public static void BuildItems(this ModelBuilder builder)
        {
            builder.InitializeBuilder<Item>();

            builder.Entity<Item>()
                .Property(x => x.Name)
                .HasMaxLength(32)
                .IsRequired();

            builder.Entity<Item>()
                .Property(x => x.Description)
                .HasMaxLength(128);

            builder.Entity<Item>()
                .Property(c => c.Colors)
                .HasConversion(
                    c => c.Select(x => x.ToString()).ToArray(),
                    c => c.Select(x => (ItemColor) Enum.Parse(typeof(ItemColor), x)).ToArray())
                .IsRequired();
        }
    }
}

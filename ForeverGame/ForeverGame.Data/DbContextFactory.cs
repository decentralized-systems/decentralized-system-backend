﻿using System;
using ForeverGame.Data.Extensions;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Npgsql;

namespace ForeverGame.Data
{
    internal class DbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        private static ApplicationDbContext Create(string basePath)
        {
            var conStrBuilder = new NpgsqlConnectionStringBuilder();
            var connectionString = conStrBuilder.ReadFromJson("developing_connection_string.json").ToString();
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException($"Connection string is null or empty.");
            }               

            var optionsBuilder =
                new DbContextOptionsBuilder<ApplicationDbContext>();

            optionsBuilder.UseNpgsql(connectionString);

            return new ApplicationDbContext(optionsBuilder.Options, new HostingEnvironment());
        }

        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { @"bin\" }, StringSplitOptions.None)[0];
            return Create(projectPath);
        }
    }
}

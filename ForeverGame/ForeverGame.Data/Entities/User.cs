﻿using System.Collections.Generic;

namespace ForeverGame.Data.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}

﻿namespace ForeverGame.Data.Entities
{
    public class Item : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ItemColor[] Colors { get; set; }

        public int Score { get; set; }

        public User User { get; set; }

        public int? UserId { get; set; }
    }

    public enum ItemColor
    {
        Red, Green, Blue, Orange
    }
}

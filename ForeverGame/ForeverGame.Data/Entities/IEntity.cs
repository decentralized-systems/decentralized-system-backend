﻿namespace ForeverGame.Data.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}

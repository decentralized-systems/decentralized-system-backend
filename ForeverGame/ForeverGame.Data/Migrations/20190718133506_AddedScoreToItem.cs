﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ForeverGame.Data.Migrations
{
    public partial class AddedScoreToItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Score",
                table: "Items",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Score",
                table: "Items");
        }
    }
}

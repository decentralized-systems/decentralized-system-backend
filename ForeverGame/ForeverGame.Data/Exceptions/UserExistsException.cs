﻿using System;

namespace ForeverGame.Data.Exceptions
{
    public class UserExistsException : Exception
    {
        public UserExistsException()
        {

        }

        public UserExistsException(string message) : base(message)
        {

        }
    }
}

﻿using System;

namespace ForeverGame.Data.Exceptions
{
    public class UnsafeUserPasswordException : Exception
    {
        public UnsafeUserPasswordException()
        {

        }

        public UnsafeUserPasswordException(string message) : base(message)
        {

        }
    }
}

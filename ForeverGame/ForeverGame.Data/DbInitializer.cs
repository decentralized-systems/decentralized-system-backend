﻿using Microsoft.EntityFrameworkCore;

namespace ForeverGame.Data
{
    public interface IDbInitializer
    {
        void Seed();
        void Migrate();
    }

    public class DbInitializer : IDbInitializer
    {
        private readonly ApplicationDbContext _dbContext;

        public DbInitializer(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }  

        public void Seed()
        {
        }

        public void Migrate()
        {
            _dbContext.Database.Migrate();
        }
    }
}
